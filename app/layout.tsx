import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import Image from "next/image";
import Link from "next/link";
import CategoryList from "@/components/CategoryList";
import BannerAd from "@/components/BannerAd";
import HiddenLinks from "@/components/HiddenLinks";
import SearchTags from "@/components/SearchTags";
import IconAd from "@/components/IconAd";
import FriendlyLinks from "@/components/FriendlyLinks";
import { SiTelegram } from "react-icons/si";
import { MdEmail } from "react-icons/md";
import Search from "@/components/Search";


const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: {
    template: '%s | 色库TV',
    default: '色库TV- 汇聚精品视频', 
  }, 
  description: "色库TV- 汇聚精品视频",
  keywords: ['真实乱伦', '精品国产', '日韩精选', '反差骚货', '激情动漫', '白虎萝莉', '网红主播'],
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="zh-CN">
      <body className={inter.className}>
        <header className="max-w-5xl mx-auto bg-white">
          <div className="container mx-auto py-4">
            <div className="flex justify-between items-center mb-4">
              <div>
                <h1 className="text-4xl font-bold text-violet-600 ml-2">色库TV</h1>
                <p className="ml-2 mt-4 text-violet-600">提示:点击查看永久收藏和无法观看电影说明</p>
              </div>
              <h2 className="mr-4 text-lg font-bold text-violet-600">我们探索的是艺术</h2>
            </div>
            <Link href="https://醘骳.sekutv.top/百度.html" target="_blank">
              <Image
                src="/fabu.gif"
                alt="Advertisement"
                width={1200}
                height={80}
                style={{
                  width: '100%',
                  height: '80px',
                  border: '1px inset #00FF00',
                }}
              />
            </Link>
          </div>
        </header>
        <CategoryList />
        <Search />
        <BannerAd />
        <HiddenLinks />
        <SearchTags />
        <IconAd />
        <FriendlyLinks />
        <main className="flex min-h-screen max-w-5xl mx-auto flex-col items-center bg-white justify-between">{children}</main>
        <footer className="max-w-5xl mx-auto bg-violet-600 rounded-lg shadow dark:bg-violet-600 m-4">
          <div className="w-full max-w-screen-xl mx-auto p-4 md:py-8">
            <div className="sm:flex sm:items-center sm:justify-between">
              <a href="/" className="flex items-center mb-4 sm:mb-0 space-x-3 rtl:space-x-reverse">
                <span className="self-center text-2xl font-semibold whitespace-nowrap dark:text-white">色库TV</span>
              </a>
              <ul className="flex flex-wrap items-center mb-6 text-sm font-medium  sm:mb-0 dark:text-gray-400">
                <li className="mr-6">
                  <div className="hover:underline flex items-center text-black hover:text-blue-500 transition">
                    <SiTelegram className="mr-2 text-2xl" />
                    <span>@sekutvkf</span>
                  </div>
                </li>
                <li>
                  <div className="hover:underline flex items-center text-black hover:text-blue-500 transition">
                    <MdEmail className="mr-2 text-2xl" />
                    <span>sekutvkf@gmail.com</span>
                  </div>
                </li>
              </ul>
            </div>
            <hr className="my-6 border-gray-200 sm:mx-auto dark:border-gray-700 lg:my-8" />
            <span className="block text-sm  sm:text-center text-black">© 2024 <a href="https://flowbite.com/" className="hover:underline">色库TV™</a>.本站已遵照「iWIN網路內容防護機構」進行分類,如有家長發現未成年兒童,少年瀏覽此站、請按照此方法過濾本站內容 &gt;{">"}『網站分級制度』</span>
          </div>
        </footer>
      </body>
    </html>
  );
}
