// 定义图标广告的接口
interface IconAd {
    id: number;             // 图片位置排序用的唯一标识符
    imageUrl: string;       // 图片链接
    title: string;          // 图片标题
    targetUrl: string;      // 图片跳转地址
    expirationDate: Date;   // 图片广告到期时间
  }
  
  // 创建图标广告数据数组
  const IconAds: IconAd[] = [
    {
      id: 1,
      imageUrl: 'https://seku5scym3q.com/logo/hq.png',
      title: '妻社',
      targetUrl: 'https://kz10.qisheqishe.com/dytz/?iuid=1005',
      expirationDate: new Date('2024-12-31'),
    },
    {
      id: 2,
      imageUrl: 'http://mossimg.xyz/LightPicture/2024/05/6a33da6c8f82b9ae.gif',
      title: 'BOBO浏览',
      targetUrl: 'https://boc405ccbbgb.shop/centerJump.html?page=bobo333-094',
      expirationDate: new Date('2024-11-30'),
    },
    {
      id: 3,
      imageUrl: 'https://seku5scym3q.com/logo/ksyp2.jpg',
      title: '快手约炮',
      targetUrl: 'https://kkstsar.zj5xqsog.cc/page.html?dc=zsqa26',
      expirationDate: new Date('2024-10-31'),
    },
    {
      id: 4,
      imageUrl: 'https://seku5scym3q.com/logo/awjd.jpg',
      title: '暗网禁地',
      targetUrl: 'https://awgdsaf.lle3yft.cc/page.html?dc=wsqa28',
      expirationDate: new Date('2024-09-30'),
    },
    {
      id: 5,
      imageUrl: 'https://seku5scym3q.com/logo/llsq.jpg',
      title: '乱伦社区',
      targetUrl: 'https://dfsdfelllhj.f1toc4eb.cc/page.html?dc=lsqa16',
      expirationDate: new Date('2024-08-31'),
    },







  ];
  export { IconAds };