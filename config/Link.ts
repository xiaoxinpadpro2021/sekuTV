interface FriendLink {
    id: number;
    url: string;
    name: string;
    openInNewTab: boolean;
  }
  
  const friendLinks: FriendLink[] = [
    {
      id: 1,
      url: 'https://汩汩一.diwrada.cc/%E5%AF%8C%E5%BC%BA',
      name: '帝王会所',
      openInNewTab: true,
    },
    {
      id: 2,
      url: 'https://鰲娩.h4j5h3.cc/4j%E9%9F%9D',
      name: '不良研究所',
      openInNewTab: false,
    },
    {
      id: 3,
      url: 'https://囔飽梦.776ddu.cc/%E5%8E%A5%E5%88%BA%E9%9F%9D',
      name: '必备福利',
      openInNewTab: true,
    },
    {
      id: 4,
      url: 'https://2酱榉.jia02dh.cc/%E5%AF%8C%E5%BC%BA',
      name: '三千佳丽',
      openInNewTab: true,
    },
    {
      id: 5,
      url: 'https://哒哒一.taygsh.cc/%E5%AF%8C%E5%BC%BA/',
      name: '麻豆福利网',
      openInNewTab: false,
    },
  ];
  export { friendLinks };