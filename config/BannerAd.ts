// 定义横幅广告的接口
interface BannerAd {
    id: number;             // 广告位置排序用的唯一标识符
    imageUrl: string;       // 广告图片链接
    title: string;          // 广告标题
    description: string;    // 广告描述
    targetUrl: string;      // 广告点击跳转地址
    expirationDate: Date;   // 广告到期时间
  }
  
  // 创建横幅广告数据数组
  const bannerAds: BannerAd[] = [
    {
      id: 1,
      imageUrl: '/17hg.gif',
      title: '皇冠117',
      description: '这是第一个横幅广告的描述',
      targetUrl: 'https://1717vip11.app/p/mFvN',
      expirationDate: new Date('2024-12-31'),
    },
    {
      id: 2,
      imageUrl: 'https://mrtoss03.com/be0ba627e78d598446af353f3fa29066.gif',
      title: '金沙',
      description: '这是第二个横幅广告的描述',
      targetUrl: 'https://38.181.224.38:2053/index.html?shareName=38.181.224.38&url',
      expirationDate: new Date('2024-11-30'),
    },
    {
      id: 3,
      imageUrl: '/9u-6.gif',
      title: '九游',
      description: '这是第三个横幅广告的描述',
      targetUrl: 'http://363jyou.com/',
      expirationDate: new Date('2024-10-31'),
    },
    {
      id: 4,
      imageUrl: 'https://888aa111bb.com/0997f404c8434991822542bd4f270e37.gif',
      title: '威尼斯',
      description: '这是第四个横幅广告的描述',
      targetUrl: 'https://s3888.vip/',
      expirationDate: new Date('2024-09-30'),
    },
  ];
  export { bannerAds };