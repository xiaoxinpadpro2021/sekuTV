// 定义分类的接口
interface Category {
    id: number;           // 分类的唯一标识符
    name: string;         // 分类名称
    url: string;          // 分类的URL
    subcategories: Category[]; // 子分类数组
  }
  
  // 创建分类数组
  const categories: Category[] = [
    {
      id: 1,
      name: '视频一区',
      url: '/electronics',
      subcategories: [
        {
          id: 1,
          name: '精品国产',
          url: '/type/smartphones',
          subcategories: [],
        },
        {
          id: 2,
          name: '日韩精选',
          url: '/electronics/laptops',
          subcategories: [],
        },
        {
          id: 3,
          name: '欧美风范',
          url: '/electronics/tablets',
          subcategories: [],
        },
        {
            id: 4,
            name: '动漫印画',
            url: '/electronics/tablets',
            subcategories: [],
          },
          {
            id: 5,
            name: '激情动漫',
            url: '/electronics/tablets',
            subcategories: [],
          },
          {
            id: 6,
            name: '经典伦理',
            url: '/electronics/tablets',
            subcategories: [],
          },
          {
            id: 7,
            name: '探花实录',
            url: '/electronics/tablets',
            subcategories: [],
          },
          {
            id: 8,
            name: '真实乱伦',
            url: '/electronics/tablets',
            subcategories: [],
          },
      ],
    },
    {
      id: 2,
      name: '视频二区',
      url: '/home-appliances',
      subcategories: [
        {
          id:9,
          name: '制服诱惑',
          url: '/home-appliances/kitchen',
          subcategories: [],
        },
        {
          id: 10,
          name: '破除见红',
          url: '/home-appliances/cleaning',
          subcategories: [],
        },
        {
          id: 11,
          name: '反差骚货',
          url: '/home-appliances/furniture',
          subcategories: [],
        },
        {
            id: 12,
            name: '白虎萝莉',
            url: '/home-appliances/furniture',
            subcategories: [],
          },
        {
            id: 13,
            name: '人妖TS',
            url: '/home-appliances/furniture',
            subcategories: [],
          },
          {
            id: 14,
            name: '网红主播',
            url: '/home-appliances/furniture',
            subcategories: [],
          },
          {
            id: 15,
            name: 'SM虐待',
            url: '/home-appliances/furniture',
            subcategories: [],
          },
          {
            id: 16,
            name: '绿帽性奴',
            url: '/home-appliances/furniture',
            subcategories: [],
          },
      ],
    },
    {
      id: 3,
      name: '精选秒播',
      url: '/clothing',
      subcategories: [
        {
          id: 31,
          name: '美女与狗',
          url: 'https://汩汩一.diwrada.cc/%E5%AF%8C%E5%BC%BA',
          subcategories: [],
        },
        {
          id: 32,
          name: '群P大战',
          url: 'https://汩汩一.diwrada.cc/%E5%AF%8C%E5%BC%BA',
          subcategories: [],
        },
        {
          id: 33,
          name: '儿子操妈',
          url: 'https://汩汩一.diwrada.cc/%E5%AF%8C%E5%BC%BA',
          subcategories: [],
        },
        {
            id: 33,
            name: '处女开苞',
            url: 'https://汩汩一.diwrada.cc/%E5%AF%8C%E5%BC%BA',
            subcategories: [],
          },
          {
            id: 33,
            name: '萝莉吞精',
            url: 'https://汩汩一.diwrada.cc/%E5%AF%8C%E5%BC%BA',
            subcategories: [],
          },
          {
            id: 33,
            name: '颜射空姐',
            url: 'https://汩汩一.diwrada.cc/%E5%AF%8C%E5%BC%BA',
            subcategories: [],
          },
          {
            id: 33,
            name: '尼姑做爱',
            url: 'https://汩汩一.diwrada.cc/%E5%AF%8C%E5%BC%BA',
            subcategories: [],
          },
          {
            id: 33,
            name: '白虎性奴',
            url: 'https://汩汩一.diwrada.cc/%E5%AF%8C%E5%BC%BA',
            subcategories: [],
          },
      ],
    },
  ];
  export { categories };