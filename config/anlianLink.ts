// 定义暗链友情链接的接口
interface HiddenFriendLink {
    id: number; // 排序用的唯一标识符
    url: string; // 链接的URL地址
    name: string; // 链接的显示名称
    openInNewTab: boolean; // 是否在新标签页中打开链接
    note: string; // 备注信息
  }
  interface PlayHiddenLink {
    id: number; // 排序用的唯一标识符
    url: string; // 视频的 vod_id
    name: string; // 链接的显示名称
    openInNewTab: boolean; // 是否在新标签页中打开链接
    note: string; // 备注信息
  }
  
  // 创建暗链友情链接数据数组
  const hiddenLinks: HiddenFriendLink[] = [
    {
      id: 1,
      url: 'https://汩汩一.diwrada.cc/%E5%AF%8C%E5%BC%BA',
      name: '乱伦',
      openInNewTab: true,
      note: '这是一个暗链友情链接',
    },
    {
      id: 2,
      url: 'https://鰲娩.h4j5h3.cc/4j%E9%9F%9D',
      name: '萝莉',
      openInNewTab: false,
      note: '这是另一个暗链友情链接',
    },
    {
      id: 3,
      url: 'https://囔飽梦.776ddu.cc/%E5%8E%A5%E5%88%BA%E9%9F%9D',
      name: '少妇',
      openInNewTab: true,
      note: '这是第三个暗链友情链接',
    },
    {
      id: 4,
      url: 'https://2酱榉.jia02dh.cc/%E5%AF%8C%E5%BC%BA',
      name: '学生',
      openInNewTab: true,
      note: '这是第四个暗链友情链接',
    },
    {
      id: 5,
      url: 'https://蔑义昷读.8xingkongav.com/%E6%BC%93%E7%A8%BD%E8%AE%8E%E5%AC%AD/a379-dk3a.html',
      name: '人妻',
      openInNewTab: false,
      note: '这是最后一个暗链友情链接',
    },
  ];
  const playHiddenLinks: PlayHiddenLink[] = [
    {
      id: 1,
      url: 'https://汩汩一.diwrada.cc/%E5%AF%8C%E5%BC%BA',
      name: '高清播放',
      openInNewTab: true,
      note: '这是一个播放暗链',
    },
    {
      id: 2,
      url: 'https://鰲娩.h4j5h3.cc/4j%E9%9F%9D',
      name: '极速播放',
      openInNewTab: false,
      note: '这是另一个播放暗链',
    },
  ];
  export { hiddenLinks, playHiddenLinks};