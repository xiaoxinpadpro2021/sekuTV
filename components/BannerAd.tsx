import React from 'react';
import Link from 'next/link';
import { bannerAds } from '@/config/BannerAd';
import Image from 'next/image';

const BannerAd: React.FC = () => {
    const currentDate = new Date();
  
    return (
      <section className="max-w-5xl mx-auto bg-white">
        <div className="flex flex-col space-y-4">
          {bannerAds.map((ad) => {
            if (ad.expirationDate >= currentDate) {
              return (
                <Link key={ad.id} href={ad.targetUrl} target="_blank" rel="noopener noreferrer">
                      <Image src={ad.imageUrl}  width={960} height={80} alt={ad.description} className="w-full h-20" loading = 'lazy'  unoptimized/>
                </Link>
              );
            }
            return null;
          })}
        </div>
      </section>
    );
  };
  
  export default BannerAd;