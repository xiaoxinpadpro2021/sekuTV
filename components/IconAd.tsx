import React from 'react';
import Link from 'next/link';
import { IconAds } from '@/config/IconAd';
import Image from 'next/image';

const IconAd: React.FC = () => {
  const currentDate = new Date();

  return (
    <section className="max-w-5xl mx-auto bg-white ">
      {/* 标题和更多链接 */}
      <div className="flex justify-between bg-purple-500 items-center ">
        <h2 className="text-lg font-bold text-white px-4 py-2 rounded">
          精品APP下载-破解版
        </h2>
        <Link href="/more" className="text-blue-500 hover:text-blue-700 hover:underline">
          点击收藏发布页
        </Link>
      </div>
      {/* 网格布局 */}
      <div className="grid grid-cols-4 sm:grid-cols-4 md:grid-cols-6 lg:grid-cols-12 gap-4 mt-4">
        {IconAds.filter((ad) => ad.expirationDate > currentDate).map((ad) => (
          <div key={ad.id} className="flex flex-col items-center">
            <Link
              href={ad.targetUrl}
              target="_blank"
              rel="noopener noreferrer"
              className="flex flex-col items-center"
            >
              <Image
                src={ad.imageUrl}
                alt={ad.title}
                width={80}
                height={80}
                className="rounded-full"
                unoptimized
              />
              <span className="mt-2 text-sm text-black font-semibold text-center">
                {ad.title}
              </span>
            </Link>
          </div>
        ))}
      </div>
    </section>
  );
};

export default IconAd;